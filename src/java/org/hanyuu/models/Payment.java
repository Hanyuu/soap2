/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.models;
import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;
/**
 *
 * @author Hanyuusha
 */
@Entity

public class Payment implements Serializable
{
    private static final long serialVersionUID = 1L;
 

   
    private int Id;
    
    private String PaymentCode;
    
    
    private Integer ReceiptId;
    
    private Integer PaymentAuthorizationCode;
    
    private Integer Sum;
    
    private String PaymenStatus;


     /**
     * @return the PaymentCode
     */
    public String getPaymentCode() {
        return PaymentCode;
    }

    /**
     * @param PaymentCode the PaymentCode to set
     */
    public void setPaymentCode(String PaymentCode) {
        this.PaymentCode = PaymentCode;
    }

    /**
     * @return the sum
     */
    public Integer getSum() {
        return Sum;
    }
     /**
     * @param sum the sum to set
     */
    public void setSum(Integer sum) {
        this.Sum = sum;
    }
    /**
     * @param PaymentAuthorizationCode the PaymentAuthorizationCode to set
     */
    public void setPaymentAuthorizationCode(int PaymentAuthorizationCode) {
        this.PaymentAuthorizationCode = PaymentAuthorizationCode;
    }

    /**
     * @return the Id
     */
    public  Integer getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return the ReceiptId
     */
    public  Integer getReceiptId() {
        return ReceiptId;
    }

    /**
     * @param ReceiptId the ReceiptId to set
     */
    public void setReceiptId(Integer ReceiptId) {
        this.ReceiptId = ReceiptId;
    }

    /**
     * @return the PaymentAuthorizationCode
     */
    public  Integer getPaymentAuthorizationCode() {
        return PaymentAuthorizationCode;
    }

    /**
     * @return the PaymenStatus
     */
    public String getPaymenStatus() {
        return PaymenStatus;
    }

    /**
     * @param PaymenStatus the PaymenStatus to set
     */
    public void setPaymenStatus(String PaymenStatus) {
        this.PaymenStatus = PaymenStatus;
    }
    
    @Override
    public String toString(){
        return  "[Payment ID="+Id+" "
                + "PaymenStatus="+PaymenStatus+" "
                + "PaymentAuthorizationCode="+PaymentAuthorizationCode+" "
                + "ReceiptId="+ReceiptId+" "
                + "PaymentCode="+PaymentCode+" "
                + "Sum="+Sum+"]";
    }
}
