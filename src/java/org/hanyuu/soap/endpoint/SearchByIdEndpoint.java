/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.soap.endpoint;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.hanyuu.dao.SoapDAO;
import org.hanyuu.models.Payment;
import org.hanyuu.soap.services.SearchByIdRequest;
import org.hanyuu.soap.services.SearchByIdResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.MessageEndpoint;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
/**
 *
 * @author Hanyuusha
 */
public class SearchByIdEndpoint implements MessageEndpoint, ApplicationContextAware {

    private ApplicationContext applicationContext;
    protected static final Logger log = Logger.getLogger(SearchEndpoint.class.getCanonicalName());
    /**
     *
     * @param mc
     */
    @Override
    public synchronized void invoke(MessageContext mc) {
        try {

            JAXBContext jbc = JAXBContext.newInstance("org.hanyuu.soap.services");
            
            Unmarshaller unMarshaller = jbc.createUnmarshaller();
           
            Marshaller marshaller = jbc.createMarshaller();
            
            SearchByIdRequest req = (SearchByIdRequest) unMarshaller.unmarshal(mc.getRequest().getPayloadSource());
           
            SearchByIdResponse res = new SearchByIdResponse();
            
            SoapDAO service = (SoapDAO) this.applicationContext.getBean(SoapDAO.class);
            
            Payment payment = service.findPaymentById(req.getId());
            if(payment != null){

               res.setIsFinded(true);
               
               res.setSum(payment.getSum());
               
               res.setPaymentCode(payment.getPaymentCode());

               switch(payment.getPaymentCode())
               {
                   case "CA":
                       res.setReceiptId(payment.getReceiptId());
                       
                       break;
                   case "acquiring":
                       res.setPaymentAuthorizationCode(payment.getPaymentAuthorizationCode());
                       break;
                           
                            
               }
               res.setOrderStatus(payment.getPaymenStatus());
               res.setIsFinded(true);
            }else{
               res.setIsFinded(false); 
               
            }
            marshaller.marshal(res, mc.getResponse().getPayloadResult());
        } catch (Exception e) {
            log.log(Level.WARNING, "[SearhById.invoke] Exception {0}", new Object[]{e});
            e.printStackTrace();
        }

    }

    /**
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}