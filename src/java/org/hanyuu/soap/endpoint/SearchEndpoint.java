/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.soap.endpoint;

/**
 *
 * @author Hanyuusha
 */
import org.hanyuu.soap.services.SearchRequest;
import org.hanyuu.soap.services.SearchResponse;
import org.hanyuu.dao.SoapDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.MessageEndpoint;
import java.util.GregorianCalendar;
import java.util.Date;
import org.hanyuu.models.Payment;
/**
 *
 * @author Hanyuu
 */
public class SearchEndpoint  implements MessageEndpoint, ApplicationContextAware {

    private ApplicationContext applicationContext;
    protected static final Logger log = Logger.getLogger(SearchEndpoint.class.getCanonicalName());
    /**
     *
     * @param mc
     */
    @Override
    public synchronized void invoke(MessageContext mc) {
        try {
            
            JAXBContext jbc = JAXBContext.newInstance("org.hanyuu.soap.services");
            
            Unmarshaller unMarshaller = jbc.createUnmarshaller();
           
            Marshaller marshaller = jbc.createMarshaller();
            
            SearchRequest req = (SearchRequest) unMarshaller.unmarshal(mc.getRequest().getPayloadSource());
           
            SearchResponse res = new SearchResponse();
            
            SoapDAO service = (SoapDAO) this.applicationContext.getBean(SoapDAO.class);
            
            String PaymentCode = req.getPaymentCode();
            
            
            int PaymentAuthorizationCode = req.getPaymentAuthorizationCode();
            
            int ReceiptId = req.getReceiptId();
            
            Payment payment = null;

            switch(PaymentCode){
                case "CA":
                    payment = service.findPayment(PaymentCode, ReceiptId, 0);
                    break;
                case "acquiring":
                    payment = service.findPayment(PaymentCode, 0, PaymentAuthorizationCode);
                    break;
                default:
                    log.log(Level.WARNING, "[Searh.invoke] Incorrect PaymentCode {0}", new Object[]{PaymentCode});
                    break;
            }
            if(payment == null){
                res.setIsFinded(false);
            }else{
               res.setIsFinded(true); 
               res.setOrderStatus(payment.getPaymenStatus());
               res.setPaymentAuthorizationCode(PaymentAuthorizationCode);
               res.setPaymentCode(PaymentCode);
               res.setReceiptId(payment.getReceiptId());
               res.setSum(payment.getSum());
            }
            
            marshaller.marshal(res, mc.getResponse().getPayloadResult());
            
        } catch (Exception e) {
            log.log(Level.WARNING, "[Searh.invoke] Exception {0}", new Object[]{e});
            e.printStackTrace();
        }

    }

    /**
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
