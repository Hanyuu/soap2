/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.soap.endpoint;
import org.hanyuu.soap.services.SearchRequest;
import org.hanyuu.models.Payment;
import org.hanyuu.dao.SoapDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.MessageEndpoint;
import java.util.GregorianCalendar;
import java.util.Date;
import org.hanyuu.models.Payment;
import org.hanyuu.soap.services.UpdateByIdRequest;
import org.hanyuu.soap.services.UpdateByIdResponse;
/**
 *
 * @author Hanyuu
 */
public class UpdateByIdEndpoint  implements MessageEndpoint, ApplicationContextAware {

    private ApplicationContext applicationContext;
    protected static final Logger log = Logger.getLogger(UpdateByIdEndpoint.class.getCanonicalName());
    @Override
   
    public synchronized void invoke(MessageContext mc) {
        
        try {
            
            JAXBContext jbc = JAXBContext.newInstance("org.hanyuu.soap.services");   
            
            Unmarshaller unMarshaller = jbc.createUnmarshaller();
            
            Marshaller marshaller = jbc.createMarshaller();
            UpdateByIdResponse res = new UpdateByIdResponse();
            UpdateByIdRequest req = (UpdateByIdRequest) unMarshaller.unmarshal(mc.getRequest().getPayloadSource());

            SoapDAO service = (SoapDAO) this.applicationContext.getBean(SoapDAO.class);

            Payment payment = service.updatePaymentById(
                    req.getRecordId(), 
                    req.getPaymentCode(), 
                    req.getReceiptId(), 
                    req.getPaymentAuthorizationCode(), 
                    req.getOrderStatus(),
                    req.getSum());
            res.setStatus(payment.toString());
            if(payment != null){
                res.setIsUpdated(true);
            }else{
                res.setIsUpdated(false);
            }
            marshaller.marshal(res, mc.getResponse().getPayloadResult());
        } catch (Exception e) {
            log.log(Level.WARNING, "[UpdateByIdEndpoint.invoke] Exception {0}", new Object[]{e});
            e.printStackTrace();
        }
    }
     /**
     *
     * @param applicationContext
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
