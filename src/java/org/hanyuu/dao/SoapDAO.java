/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.dao;


import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import java.util.List;

import org.hibernate.classic.Session;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import java.util.Date;
import org.hibernate.Transaction;
import org.hanyuu.models.Payment;
/**
 *
 * @author Hanyuu
 */
public class SoapDAO extends HibernateDaoSupport {

    private static final Logger log = Logger.getLogger(HibernateDaoSupport.class.getCanonicalName());


    public Payment findPayment(String PaymentCode, int ReceiptId, int PaymentAuthorizationCode) {
        Session session = this.getSessionFactory().openSession();
        
        log.log(Level.INFO, "[SoapDAO.findPayment] PaymentCode = {0}  PaymentAuthorizationCode = {1} ReceiptId = {2}", new Object[]{PaymentCode,PaymentAuthorizationCode,ReceiptId});
        
        Payment payment = (Payment)session
                .createQuery("from Payment where PaymentCode=:PaymentCode AND ReceiptId=:ReceiptId AND PaymentAuthorizationCode=:PaymentAuthorizationCode")
                .setString("PaymentCode", PaymentCode)
                .setInteger("ReceiptId", ReceiptId)
                .setInteger("PaymentAuthorizationCode",PaymentAuthorizationCode)
                .uniqueResult();
        
        if (payment != null) {
           
            log.log(Level.INFO, "[SoapDAO.findPayment] Finded Payment id={0}", new Object[]{payment});
            
            return payment;
        } else {
            
            log.log(Level.INFO, "[SoapDAO.findPayment] No Payments found", new Object[]{});
            
            return null;
        }
    }
    
    public Payment findPaymentById(int Id) {
        Session session = this.getSessionFactory().openSession();
        
        log.log(Level.INFO, "[SoapDAO.findPayment] try found Payment id={0}", new Object[]{Id});
        
        Payment payment = (Payment)session
                .createQuery("from Payment where id=:id")
                .setInteger("id", Id)
                .uniqueResult();
        
        if (payment != null) {
            
            log.log(Level.INFO, "[SoapDAO.findPaymentById] Finded Payment ID={0}", new Object[]{payment.getId()});
           
            return payment;
        } else {
            
            log.log(Level.INFO, "[SoapDAO.findPayment] No Payments found id={0}", new Object[]{Id});
            
            return null;
        }
    }
    
    public Payment updatePaymentById(int Id, String PaymentCode, Integer ReceiptId, Integer PaymentAuthorizationCode,String PaymenStatus,Integer sum) {
        Session session = this.getSessionFactory().openSession();
        
        log.log(Level.INFO, "[SoapDAO.updatePaymentById] try found Payment id={0}", new Object[]{Id});
        
        Payment payment = (Payment)session
                .createQuery("from Payment where id=:id")
                .setInteger("id", Id)
                .uniqueResult();
        
        if (payment != null) {
            
            log.log(Level.INFO, "[SoapDAO.updatePaymentById] Finded Payment ID={0}", new Object[]{payment.getId()});
            
            if(PaymentCode != null){
                payment.setPaymentCode(PaymentCode);
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymentCode={0} Payment ID={1}", new Object[]{PaymentCode,payment.getId()});
            }else{
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymentCode is null Payment ID={0}", new Object[]{payment.getId()});
            }
            
            if(ReceiptId != null){
                payment.setReceiptId(ReceiptId);
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated ReceiptId={0} Payment ID={1}", new Object[]{ReceiptId,payment.getId()});
            }else{
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated ReceiptId is null Payment ID={0}", new Object[]{payment.getId()});
            }
            
            if(PaymentAuthorizationCode != null){
                payment.setPaymentAuthorizationCode(PaymentAuthorizationCode);
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymentAuthorizationCode={0} Payment ID={1}", new Object[]{PaymentAuthorizationCode,payment.getId()});
            }else{
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymentAuthorizationCode is null Payment ID={0}", new Object[]{payment.getId()});
            }
             if(sum != null){
                payment.setSum(sum);
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated Sum={0} Payment ID={1}", new Object[]{sum,payment.getId()});
            }else{
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated Sum is null Payment ID={0}", new Object[]{payment.getId()});
            }
            
            if(PaymenStatus != null){
                payment.setPaymenStatus(PaymenStatus);
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymenStatus={0} Payment ID={1}", new Object[]{PaymenStatus,payment.getId()});
            }else{
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Updated PaymenStatus is null Payment ID={0}", new Object[]{payment.getId()});
            }
            Transaction transaction = session.beginTransaction();
            try{
                log.log(Level.INFO,"[SoapDAO.updatePaymentById] Saving record {0}",new Object[]{payment.getId()});
                
                transaction.begin();
                session.flush();
                session.save(payment);
                transaction.commit();
                log.log(Level.INFO,"[SoapDAO.updatePaymentById] Saved record {0}",new Object[]{payment.getId()});
            }catch(Exception ex){
                log.log(Level.INFO, "[SoapDAO.updatePaymentById] Exception {0} while saving {1]", new Object[]{ex.getMessage(),payment.getId()});
                transaction.rollback();
            }
            return payment;
        } else {
            
            log.log(Level.INFO, "[SoapDAO.updatePaymentById] No Payments found id={0}", new Object[]{Id});
            
            return null;
        }
    }

}
